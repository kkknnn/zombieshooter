#include <QApplication>
#include <QObject>
#include <QGraphicsScene>
#include "include/player.h"
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QTimer>
#include <stdlib.h>
#include <iostream>
#include "include/gameholder.h"


GameHolder * game;


int main(int argc, char *argv[])
{

    srand(static_cast<unsigned int>(time(nullptr)));
    QApplication a(argc, argv);

    game= new GameHolder;
    game->show();


    return a.exec();
}
