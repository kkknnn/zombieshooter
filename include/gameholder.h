#ifndef GAMEHOLDER_H
#define GAMEHOLDER_H

#include <QGraphicsView>
#include "player.h"
#include <QGraphicsScene>
#include <QWidget>


class GameHolder: public QGraphicsView
{
public:
    GameHolder(QWidget * parent=nullptr);

private:
    myPlayer * player;
    QGraphicsScene * scene;
    int WindowResX;
    int WindowResY;
};

#endif // GAMEHOLDER_H
