#ifndef PLAYER_H
#define PLAYER_H
#include <QGraphicsRectItem>
#include <QObject>
#include "../include/enemy.h"



class myPlayer: public QObject,public QGraphicsRectItem
{
    Q_OBJECT
public:
    myPlayer()=default;
    void keyPressEvent(QKeyEvent * event) override;

public slots:
    void spawn();

private:

};

#endif // PLAYER_H
